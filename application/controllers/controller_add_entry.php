<?php

class Controller_Add_Entry extends Controller {

    function __construct() {
        $this->view = new View();
        $this->model = new Model_Entry();
    }

    public function action_index() {

        if(!$this->GetRights()){
            header('location: /');
            exit();
        }

        if(!empty($_POST)){
            $this->model->add_new_entry($_POST);
            $new_entry_id = $this->model->get_last_id();
            header("location: /read/$new_entry_id");
            exit();
        }

        $twig_data = array();
        $twig_data['header'] = $this->GetHeader();
        $twig_data['sections'] = $this->model->get_sections();;

        $this->view->generate( 'templates/blog_new_entry.html.twig', $twig_data );

    }
}