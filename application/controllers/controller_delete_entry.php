<?php

class Controller_Delete_Entry extends Controller {

    function __construct() {
        $this->view = new View();
        $this->model = new Model_Entry();
    }

    public function action_index($delete_entry_id) {

        if(!$this->GetRights()){
            header('location: /');
            die;
        }

        $this->model->delete_entry($delete_entry_id);
        header('location: /');

    }
}