<?php

class Controller_Edit_Sections extends Controller{

    public function __construct(){
        $this->model = new Model_Section();
        $this->view = new View();
    }

    public function action_index(){

        if(!empty($_POST)){
            if($_POST['action'] == 'add'){
                unset($_POST['action']);
                $this->model->add_new_section($_POST);
            } else {
                $this->model->delete_section((int)$_POST['action']);
            }
        }

        $sections_data = $this->model->get_sections();


        $twig_data = array(
            'sections' => $sections_data
        );

        $twig_data['header'] = $this->GetHeader();

        $this->view->generate( 'templates/blog_edit_sections_view.html.twig', $twig_data );

    }
}