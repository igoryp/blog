<?php

class Controller_Main extends Controller {

    function __construct() {
        $this->view = new View();
        $this->model = new Model_Entry();
    }

    public function action_index($section = null) {

        $posts_data = $this->model->get_entries($section);

        $sections_data = $this->model->get_sections();

        $twig_data = array('posts' => $posts_data, 'sections' => $sections_data);

        $twig_data['admin'] = $this->GetRights();

        $twig_data['header'] = $this->GetHeader();

        $this->view->generate( 'templates/blog_main_list_view.html.twig', $twig_data );
    }
}