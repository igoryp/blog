<?php

class Controller_Auth extends Controller {

    function __construct() {
        $this->view = new View();
        $this->model = new Model_Auth();
    }

    public function action_index() {

        $error_msg = '';

        if(isset($_POST['enter'])) {

            if (preg_match("/^[a-zA-Z0-9_]{5,30}$/", $_POST['login']) && preg_match("/^[a-zA-Z0-9_]{5,30}$/", $_POST['password'])) {

                $user = $this->model->get_user_by_login($_POST['login']);

                if ($_POST['login'] === $user['login'] && $_POST['password'] === $user['password']) {

                    $_SESSION['admin'] = true;
                    $_SESSION['login'] = $user['login'];

                    header('Location: /');
                }
                else {
                    $error_msg =  "Not Found This User!";
                }
            }
            else {
                $error_msg = "Error!";
            }

        }

        $header =  'templates/blog_menu_view.html.twig';

        $twig_data = array('header' => $header, 'error_msg' => $error_msg );

        $this->view->generate( 'blog_login_view.html.twig', $twig_data );

    }
}