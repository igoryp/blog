<?php

class Controller_Exit extends Controller {

    public function action_index(){
        session_destroy();
        header('location: /');
    }
}