<?php

class Controller_about extends Controller{
    public function __construct(){
        $this->view = new View();
    }

    public function action_index(){

        $twig_data['header'] = $this->GetHeader();

        $this->view->generate( 'templates/blog_about_view.html.twig', $twig_data );

    }
}