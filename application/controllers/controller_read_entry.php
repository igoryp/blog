<?php

class Controller_Read_Entry extends Controller {

    public function __construct(){
        $this->model = new Model_Entry();
        $this->view = new View();
    }


    public function action_index($read_entry_id){

        $data = $this->model->get_entry((int)$read_entry_id);

        if(empty($data)){
            header('location: /');
            exit();
        }

        if(isset($this->get_array[2]) && $this->GetRights()){

            if($this->get_array[2] == 'show') $this->model->show_comment((int)$this->get_array[3]);
            elseif($this->get_array[2] == 'delete') $this->model->delete_comment((int)$this->get_array[3]);
            elseif($this->get_array[2] == 'hide') $this->model->hide_comment((int)$this->get_array[3]);

            header('location: /read/'.(int)$read_entry_id.'/');
        }

        $sections_data = $this->model->get_sections();



        $comments = $this->model->get_comments((int)$read_entry_id, $this->GetRights());

        $twig_data = array(
            'id'=>$data[0]['id'],
            'entry_text'=>$data[0]['entry_text'],
            'entry_title'=>$data[0]['entry_title'],
            'entry_description'=>$data[0]['entry_description'],
            'entry_section'=>$data[0]['entry_section'],
            'entry_date'=>$data[0]['entry_date'],
            'entry_comments_count' => $data[0]['entry_comments_count'],
            'sections' => $sections_data,
            'comments' => $comments

        );

        if(!empty($_POST)){
            $date = new DateTime('NOW');
            $_POST['date'] = $date->format('Y-m-d H:i');
            $_POST['entry_id'] = $read_entry_id;
            $this->model->add_comment($_POST);
            $twig_data['message'] = 'Cпасибо за комментарий!';
        }


        $twig_data['header'] = $this->GetHeader();

        $twig_data['admin'] = $this->GetRights();

        $this->view->generate( 'templates/blog_read_entry.html.twig', $twig_data );

    }

}