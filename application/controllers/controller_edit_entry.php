<?php

class Controller_Edit_Entry extends Controller {

    function __construct() {
        $this->view = new View();
        $this->model = new Model_Entry();
    }

    public function action_index($edit_entry_id) {

        if(!$this->GetRights()){
            header('location: /');
            exit();
        }

        if(!empty($_POST)){
            $this->model->update_entry($_POST);
            header("location: /read/$edit_entry_id");
            exit();
        }

        $data = $this->model->get_entry($edit_entry_id);

        $sections_data = $this->model->get_sections();

        $twig_data = array(
            'id'=>$data[0]['id'],
            'entry_text'=>$data[0]['entry_text'],
            'entry_title'=>$data[0]['entry_title'],
            'entry_description'=>$data[0]['entry_description'],
            'entry_section'=>$data[0]['entry_section'],
            'sections' => $sections_data,
            'edit_entry_id' => $edit_entry_id
        );

        $twig_data['header'] = 'templates/blog_menu_view_admin.html.twig';

        $this->view->generate( 'templates/blog_new_entry.html.twig', $twig_data );

    }

}