<?php

require_once 'core/config.php';

require_once 'core/DB_Connect.php';

require_once 'core/controller.php';
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/route.php';

require_once 'vendor/autoload.php';


Route::start();