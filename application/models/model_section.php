<?php

class Model_Section extends Model {

    public function add_new_section($query_string){

        $query = "INSERT INTO `blog_sections` (";

        foreach ($query_string as $key => $value) {
            $query .= "`" . $key . "`,";
        }

        $query = substr($query, 0, -1);
        $query .= ") VALUES (";

        foreach ($query_string as $key => $value) {
            $query .= ":" . $key . ",";
        }

        $query = substr($query, 0, -1);
        $query .= ")";

        DB_Connect::getInstance()->request($query, $query_string);
    }

    public function delete_section($id){
        $query = "DELETE FROM `blog_sections` WHERE `id` = :id";
        $data['id'] = $id;
        DB_Connect::getInstance()->request($query, $data);
    }

}