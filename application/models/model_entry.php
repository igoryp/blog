<?php

class Model_Entry extends Model {

    public function get_entries($section) {

        $entry_section =($section)? 'WHERE `entry_section` = '.(int)$section : '';

        $query = "SELECT `blog_entries`.`id`, `entry_title`, `entry_text`,
         `entry_description`, `entry_date`, DATE_FORMAT(`entry_date`,'%d-%m-%y %H:%i') AS `entry_date_text`,
         COUNT(  `blog_comments`.id ) AS  `entry_comments_count`
         FROM `blog_entries`
         LEFT JOIN  `blog_comments` ON  `blog_comments`.`entry_id` =  `blog_entries`.`id`
         AND  `blog_comments`.`active` =  '1'
         $entry_section
         GROUP BY  `blog_entries`.`id`
         ORDER BY `entry_date` DESC";

        $data = DB_Connect::getInstance()->select_request($query);

        return $data;
    }

    public function get_entry($id) {
        $query = "SELECT  `blog_entries` . * , DATE_FORMAT(  `entry_date` ,  '%d-%m-%y %H:%i' ) AS  `entry_date` , COUNT(  `blog_comments`.id ) AS  `entry_comments_count`
                FROM  `blog_entries`
                LEFT JOIN  `blog_comments` ON  `blog_comments`.`entry_id` =  `blog_entries`.`id`
                AND  `blog_comments`.`active` =  '1'
                AND  `blog_comments`.`entry_id` = '". $id ."'
                WHERE  `blog_entries`.`id` = '". $id ."'
                GROUP BY  `blog_entries`.`id` ";

        $data = DB_Connect::getInstance()->select_request($query);

        return $data;
    }

    public function add_new_entry($query_string) {
        $query = "INSERT INTO `blog_entries` (";

        foreach ($query_string as $key => $value) {
            $query .= "`" . $key . "`,";
        }

        $query = substr($query, 0, -1);
        $query .= ") VALUES (";

        foreach ($query_string as $key => $value) {
            $query .= ":" . $key . ",";
        }

        $query = substr($query, 0, -1);
        $query .= ")";

        DB_Connect::getInstance()->request($query, $query_string);
    }

    public function update_entry($query_string) {

        $query = "UPDATE `blog_entries` SET ";

        foreach ($query_string as $i => $value) {
            $query .= "`" . $i . "` = :" . $i . ",";
        }

        $query = substr($query, 0, -1);

        $query .= " WHERE `id` = '" . $query_string['id'] . "'";

        DB_Connect::getInstance()->request($query, $query_string);
    }

    public function delete_entry($id) {
        $query = "DELETE FROM `blog_entries` WHERE `id` = :id";
        $param['id'] = $id;
        DB_Connect::getInstance()->request($query, $param);
    }

    public function add_comment($query_string) {
        $query = "INSERT INTO `blog_comments` (";

        foreach ($query_string as $key => $value) {
            $query .= "`" . $key . "`,";
        }

        $query = substr($query, 0, -1);
        $query .= ") VALUES (";

        foreach ($query_string as $key => $value) {
            $query .= ":" . $key . ",";
        }

        $query = substr($query, 0, -1);
        $query .= ")";

        DB_Connect::getInstance()->request($query, $query_string);
    }

    public function get_comments($id, $rights) {
        $where = '';
        if(!$rights){
            $where = "AND `active` = '1'";
        }
        $query = "SELECT *, DATE_FORMAT(`date`,'%d-%m-%y %H:%i') AS `date` FROM `blog_comments` WHERE `entry_id` = ". $id ." {$where}";

        $data = DB_Connect::getInstance()->select_request($query);

        return $data;
    }

    public function show_comment($id){
        $query = "UPDATE `blog_comments` SET `active` = '1' WHERE `id` = :id";
        $param['id'] = $id;
        DB_Connect::getInstance()->request($query, $param);
    }

    public function hide_comment($id){
        $query = "UPDATE `blog_comments` SET `active` = '0' WHERE `id` = :id";
        $param['id'] = $id;
        DB_Connect::getInstance()->request($query, $param);

    }

    public function delete_comment($id){
        $query = "DELETE FROM `blog_comments` WHERE `id` = :id";
        $param['id'] = $id;
        DB_Connect::getInstance()->request($query, $param);
    }

    public function get_last_id() {
        $data = DB_Connect::getInstance()->get_last_insert_id();
        return $data;
    }

}