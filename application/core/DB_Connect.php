<?php

class DB_Connect {
    private static $dbInstance;
    private $dbConnection;

    private function __construct() {
        try {
            $this->dbConnection = new PDO('mysql:host=' . HOST_NAME . ';dbname=' . DB_NAME, USER_NAME, PASSWORD);
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbConnection->exec('SET NAMES utf8');
            $this->dbConnection->exec('SET CHARACTER SET utf8');
            $this->dbConnection->exec('SET COLLATION_CONNECTION="utf8_general_ci"');
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            die;
        }
    }

    public static function getInstance() {
        if (self::$dbInstance === null) {
            self::$dbInstance = new self();
        }

        return self::$dbInstance;
    }

    public function select_request($query) {
        $dbh = self::getInstance();

        $sth = $dbh->dbConnection->query($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $data = array();
        while ($row = $sth->fetch()) {
            $data[] = $row;
        }

        return $data;
    }

    public function request($query, $param) {
        $dbh = self::getInstance();

        $sth = $dbh->dbConnection->prepare($query);
        $sth->execute($param);

    }

    public function get_last_insert_id() {
        return $this->dbConnection->lastInsertId();
    }
}