<?php

class Model {

    function __construct() {

    }

    public function get_sections() {

        $query = "select `s`.*, count( `e`.id  ) as `count` from blog_sections as `s`
                  LEFT join blog_entries as `e` on `s`.id = `e`.entry_section
                  GROUP BY `s`.id";

        $data = DB_Connect::getInstance()->select_request($query);

        return $data;
    }
}