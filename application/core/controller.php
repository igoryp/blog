<?php

class Controller {
    public $view;
    public $model;
    public $twig;
    public $get_array;


    function __construct() {
        $this->view = new View();

    }

    protected function GetRights(){

        return (isset($_SESSION['admin'])) ? true : false ;

    }

    protected function GetHeader(){

        return (isset($_SESSION['admin'])) ? "templates/blog_menu_view_admin.html.twig" : "templates/blog_menu_view.html.twig" ;

    }

}