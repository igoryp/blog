<?php

class View {

    protected $twig;

    public  function __construct(){
        $this->twig = new Twig_Environment(  new Twig_Loader_Filesystem(TEMPLATE_PATH) , array('autoescape' => false));
    }

    protected  function GetTwig(){
        return $this->twig;
    }

    function generate( $template_view, $twig_data) {

        $template = $this->GetTwig()->loadTemplate($template_view);

        $posts = $template->render($twig_data);

        echo $posts;
    }

}