<?php

class Route {

    private function __construct() { }

    private static function includeControllerAndModel($controller_name, $model_name) {

        $controller = strtolower($controller_name);
        $model = strtolower($model_name);

        if (file_exists(CONTROLLER_PATH . $controller . '.php')) {
            include CONTROLLER_PATH . $controller . '.php';
        }
        else {
            self::errorPage404();

            die;
        }

        if (file_exists(MODEL_PATH . $model . '.php')) {
            include MODEL_PATH . $model . '.php';
        }
    }

    public static function start() {

        $get = null;
        $controller_name = '';
        $param = '';

        if (!empty($_GET['route'])) {
            $get = explode('/', trim($_GET['route'], '/'));

            $page = $get[0];

            if (!empty($get[1]))
                $param = $get[1];
            else
                $param = null;
        }
        else
            $page = 'main';

        switch ($page) {
            case 'auth':

                $controller_name = 'Controller_Auth';
                $model_name = 'Model_Auth';
                break;

            case 'about':

                $controller_name = 'Controller_about';
                $model_name = '';
                break;

            case 'resume':

                header('location: /pdf/pershin.pdf');
                exit();
                break;

            case 'main':

                $controller_name = 'Controller_Main';
                $model_name = 'Model_Entry';
                break;

            case 'read':
                if ($param == null)
                    self::errorPage404();

                $controller_name = 'Controller_Read_Entry';
                $model_name = 'Model_Entry';
                break;

            case 'add':
                $controller_name = 'Controller_Add_Entry';
                $model_name = 'Model_Entry';
                break;

            case 'sections':
                $controller_name = 'Controller_Edit_Sections';
                $model_name = 'Model_Section';
                break;

            case 'edit':
                $controller_name = 'Controller_Edit_Entry';
                $model_name = 'Model_Entry';
                break;

            case 'delete':
                $controller_name = 'Controller_Delete_Entry';
                $model_name = 'Model_Entry';
                break;

            case 'exit':
                $controller_name = 'Controller_Exit';
                $model_name = '';
                break;

            default:
                $controller_name = 'Controller_Main';
                $model_name = 'Model_Entry';
                break;
        }

        self::includeControllerAndModel($controller_name, $model_name);

        $pas = 'action_index';
        $controller = new $controller_name;
        $controller->get_array = $get;
        $controller->$pas($param);
    }

    public static function errorPage404() {
        header("HTTP/1.1: 404 Not Found");
        echo '<h1>404</h1>';
        die;
    }
}